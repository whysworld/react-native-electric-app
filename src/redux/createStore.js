import { createStore } from 'redux';

import reducers from './reducers';

export default (data = {}) => {
    const store = createStore(reducers, data);
    return store;
}
