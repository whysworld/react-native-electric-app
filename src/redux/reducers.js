import { combineReducers } from 'redux';

// Load saved state values from local storage.

// Default state values
var initialLoginState = false;
var initialTokenState = "";

var loginState = null;

// This reducer contains the login state.   ture or false.
const loginReducer = (state = false, action) => {
    switch (action.type) {
      case 'SIGNIN':
      console.log("signed in");
        state = true;
        break;
      case 'SIGNOUT':
        state = false;
        break;
      default:
    }
    return state;
}

// This reducer contains the token value.
const tokenReducer = (state = {}, action) => {
    switch (action.type) {
      case 'SETTOKEN':
        state = action.token;
        break;
      case 'CLRTOKEN':
        state = "";
        break;
      default:
    }
    return state;
}

// This reducer contains the token value.
const userReducer = (state = "", action) => {
  switch (action.type) {
    case 'SETUSERINFO':
      state = action.info;
      break;
    case 'CLRUSERINFO':
      state = null;
      break;
    default:
  }
  return state;
}

  
export default combineReducers({loginReducer, tokenReducer, userReducer});   // export reducers.