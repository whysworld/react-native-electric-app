export const actSetToken = token => ({         // token value set action
    type: 'SETTOKEN',
    token
  })
  
export const actClrToken = () => ({            // token value clear action
    type: 'CLRTOKEN'
  })