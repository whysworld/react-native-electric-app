import * as LoginActions from './login';
import * as TokenActions from './token';
import * as InfoActions from './info';

export const ActionCreators = Object.assign({},
    LoginActions,
    TokenActions,
    InfoActions
);