export const actSetUserInfo = info => ({         // token value set action
    type: 'SETUSERINFO',
    info
  })
  
export const actClrUserInfo = () => ({            // token value clear action
    type: 'CLRUSERINFO'
  })