export const actSignIn = () => ({              // sign in action
    type: 'SIGNIN'
  })
  
export const actSignOut = () => ({             // sign out action
    type: 'SIGNOUT'
})