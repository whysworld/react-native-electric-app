import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity, Image, ImageBackground, Alert } from 'react-native'
import { Text, Input, Button } from 'react-native-elements'
import { ActionCreators } from '../../redux/actions/index'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import API from '../../api/api'
import config from '../../api/config'
import images from '../../utils/images'

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: 'demo@demo.com',
            password: 'electrician'
        }
        this.onSignIn = this.onSignIn.bind(this);
        this.onSignUp = this.onSignUp.bind(this);
        // console.disableYellowBox = true;
        // console.ignoredYellowBox = ['Warning:'];
    }

    componentWillUnmount() {

    }

    // onSignIn = () => {
    //     console.log("SignIn pressed");
    //     API.post("/auth/login", {
    //         'email' : this.state.email,
    //         'password' : this.state.password
    //     }).then(res => {
    //         console.log(res);
    //         const {actSetUserInfo, actSetToken, actSignIn} = this.props;
    //         if (res.status == 201) {
    //             actSetUserInfo(res.data.decodedToken);
    //             actSetToken(res.data.access_token);
    //             actSignIn();
    //             this.props.navigation.navigate('HomeScreen');
    //         }
    //     }).catch(error => {
    //         this.showAlert('Login failed.')
    //         console.log(error);
    //     })

    // }
    onSignIn = async () => {
        try {
            var res = await API.post("/auth/login",{
                'email' : this.state.email,
                'password' : this.state.password
            });
            // this.props.navigation.navigate('HomeScreen');
            
            if (res.status == 201) {
                const {actSetUserInfo, actSetToken, actSignIn} = this.props;
                actSetUserInfo(res.data.decodedToken);
                actSetToken(res.data.access_token);
                actSignIn();
                // this.showAlert(res.status.toString());
                this.props.navigation.navigate('HomeScreen');
            }
        } catch(error) {
            // console.log(error);
        }

    }

    onSignUp = () => {
        this.props.navigation.navigate("SignUpScreen");
        return;
    }

    showAlert(strMsg) {
        Alert.alert(
        'Handbook',
        strMsg,
        [{text: 'OK', onPress: () => console.log('OK Pressed')},],
        { cancelable: true }
        );
    }

    handleLogDataInput = (data) => {
        // const {}
        this.setState({"email": data });
        console.log(data);
    }
    
    handlePasswordInput = (data) => {
        this.setState({"password": data});
    }

    render() {
        const {email, password} = this.state;
        return (
            <ImageBackground source={images.loginBackground} style={{width:'100%', height:'100%'}}>
            <View style={styles.container}>
                <ImageBackground imageStyle={{resizeMode: 'stretch'}} source={images.loginTextBackground} style={styles.descriptionContainer}>
                    <Text style={styles.descriptionText}>SignIn with existing account.</Text>
                </ImageBackground>

                <View style={styles.loginForm}>
                    <Input containerStyle = {styles.input} 
                        value={email}
                        autoCapitalize="none" 
                        keyboardType='email-address' 
                        returnKeyType="next" 
                        placeholder='Email or Phone Number'
                        placeholderTextColor='#CCCCCC'
                        inputContainerStyle={{borderColor: "transparent"}}
                        inputStyle={{ color: 'white', fontSize: 14 }}
                        onChangeText={(data) => {this.handleLogDataInput(data)}}
                        leftIcon={{ type: 'font-awesome', name: 'envelope', color: 'white',marginRight: 6 }}/>

                    <Input containerStyle = {styles.input}   
                        value={password}
                        returnKeyType="go" 
                        placeholder='Password' 
                        placeholderTextColor='#CCCCCC' 
                        secureTextEntry
                        underlineColorAndroid="transparent"
                        inputContainerStyle={{borderColor: "transparent"}}
                        inputStyle={{ color: 'white', fontSize: 14 }}
                        onChangeText={(data) => {this.handlePasswordInput(data)}}
                        leftIcon={{ type: 'font-awesome', name: 'lock', color: 'white',marginRight: 6 }}/>

                    <Button buttonStyle={styles.signin} title="SIGN IN"  onPress={this.onSignIn.bind(this)}/>

                    <Button buttonStyle={styles.back} title="BACK" onPress={() => this.props.navigation.goBack()}/>

                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 40 }}>
                        <Text style={{ color: "#CCCCCC" }}>Do not have an account?  </Text>
                        <TouchableOpacity onPress={this.onSignUp.bind(this)}>
                            <Text style={{ color: "white", fontSize: 15 }}>Sign Up</Text>
                        </TouchableOpacity>
                    </View>

                </View>
            </View>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
    },
    descriptionContainer: {
        marginTop: 20,
        padding: 20,
    },
    descriptionText: {
        color: '#000000',
        fontSize: 30
    },
    loginForm: {
        width: '100%',
        padding: 50,
    },
    input:{
        height: 40,
        backgroundColor: 'rgba(128, 128, 128, 0.7)',
        marginBottom: 30,
        width: '100%',
        borderRadius: 10
    },
    signin:{
        backgroundColor: '#FE9723',
        marginBottom: 10,
        marginTop: 10
    },
    back:{
        backgroundColor: '#23ff89',
        marginBottom: 20,
        marginTop: 10
    },
    buttonText:{
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
    }
});

const mapStateToProps = store => ({
    tokenValue: store.tokenReducer,
    userValue: store.userReducer
});
  
const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(ActionCreators, dispatch);
}
  
export default connect(mapStateToProps, mapDispatchToProps)(Login);