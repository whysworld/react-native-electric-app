//import liraries
import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  Platform,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';
import { width, height } from 'react-native-dimension';
import { NavigationActions } from 'react-navigation';
import AwesomeAlert from 'react-native-awesome-alerts';
// REDUX
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ActionCreators } from '../../redux/actions/index';

import images from '../../utils/images';
import config from '../../api/config';

// create a component
class Side_Bar extends Component {
  constructor(props) {
    super(props);

    this.handleLogout = this.handleLogout.bind(this);

    this.handleLogIn = this.handleLogIn.bind(this);
  }

  componentWillMount() {
    
  }
  
  componentWillUnmount() {
  }

  handleLogout = () => {
    const {actSignOut, actClrToken, actClrUserInfo} = this.props;
    actClrToken();
    actClrUserInfo();
    actSignOut();
  }
  handleLogIn = () => {
    this.props.navigation.navigate('LoginScreen');
  }

  render() {
    const {loginReducer, userInfo} = this.props;
    return (

      <ImageBackground source={images.sidebarBackground} style={styles.container}>
        <View style={styles.sideBarHeader}>
          <Image
          source={images.userAvatar}
          style={styles.userAvatar} 
          />
          { 
            loginReducer && (
              <Text style={styles.menuLabel}>{userInfo.userName}</Text>
          )
          }
        </View>
        <View style={styles.menuContainer}>
          <TouchableOpacity style={styles.menuItem}>
            <Image
              source={images.sidebarIconHome}
              style={styles.menuIcon}
              resizeMode="center"
            />
            <Text style={styles.menuLabel}>Home</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.menuItem}>
            <Image
              source={images.sidebarIconSetting}
              style={styles.menuIcon}
            />
            <Text style={styles.menuLabel}>Settings</Text>
          </TouchableOpacity>
          {
            loginReducer && (
              <TouchableOpacity style={styles.menuItem}>
                <Image
                  source={images.sidebarIconLogout}
                  style={styles.menuIcon}
                />
                <Text style={styles.menuLabel} onPress={this.handleLogout.bind(this)}>Logout</Text>
              </TouchableOpacity>
            )
          }          
          {
            !loginReducer && (
              <TouchableOpacity style={styles.menuItem} onPress={this.handleLogIn.bind(this)}>
                <Image
                  source={images.sidebarIconProfile}
                  style={styles.menuIcon}
                />
                <Text style={styles.menuLabel}>Login</Text>
              </TouchableOpacity>
            )
          }          
        </View>
      </ImageBackground>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  sideBarHeaderText: {
    height: 50,
    width: '100%',
    alignItems: 'center'
  },
  sideBarHeader: {
    flex: 0.3,
    alignItems: 'center'
  },
  menuContainer: {
    flex: 1,
    // backgroundColor: '#140f1b',
    flexDirection: 'column'
  },
  userAvatar: {
    marginTop: 50,
    width: 145,
    height: 140,
  },
  menuItem: {
    height: 80,
    paddingTop: 20,
    paddingBottom: 20,
    flexDirection: 'row',
    borderTopWidth: 2,
    borderTopColor: '#aaaaaa'
  },
  menuIcon: {
    marginLeft: 20,
    width: 40,
    height: 40
  },
  menuLabel: {
    marginLeft: 20,
    paddingTop: 5,
    color: '#fff',
    fontSize: 22,
    justifyContent: 'center'
  }
});

const mapStateToProps = store => ({
  userInfo: store.userReducer,
  loginReducer: store.loginReducer,
})

const mapDispatchToProps = (dispatch) => {
   return bindActionCreators(ActionCreators, dispatch);
}

//make this component available to the app
export default connect(mapStateToProps, mapDispatchToProps)(Side_Bar);
