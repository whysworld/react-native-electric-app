import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity, Image, ImageBackground, Alert } from 'react-native'
import { Text, Input, Button } from 'react-native-elements'
import { ActionCreators } from '../../redux/actions/index'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import API from '../../api/api'
// import config from '../../api/config'
import images from '../../utils/images'

class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: 'test1',
            email: 'test1@test1.com',
            password: 'electrician',
            repassword: 'electrician'
        }
        this.onSignUp = this.onSignUp.bind(this);
        // console.disableYellowBox = true;
        // console.ignoredYellowBox = ['Warning:'];
    }

    componentWillUnmount() {

    }

    onSignUp = async () => {
        try {
            var res = await API.post("/auth/register",{
                'userName' : this.state.username,
                'email' : this.state.email,
                'password' : this.state.password,
                'role' : "user",
                'account_status' : "Active"
            });
            if (res.status == 201) {
                if (res.data.success == true) {
                    this.showAlert("Registered successfully.");
                    this.props.navigation.navigate('LoginScreen');
                } else {
                    this.showAlert("This email is already registered");
                }
            }
        } catch(error) {
            this.showAlert("Error while registering request.");
        }
    }

    showAlert(strMsg) {
        Alert.alert(
        'Handbook',
        strMsg,
        [{text: 'OK', onPress: () => console.log('OK Pressed')},],
        { cancelable: true }
        );
    }

    handleLogUserNameInput = (data) => {
        this.setState({"username": data });
    }
    handleLogDataInput = (data) => {
        this.setState({"email": data });
    }
    
    handlePasswordInput = (data) => {
        this.setState({"password": data});
    }
    handleRetypePasswordInput = (data) => {
        this.setState({"repassword": data});
    }

    render() {
        const {username, email, password, repassword} = this.state;
        return (
            <ImageBackground source={images.loginBackground} style={{width:'100%', height:'100%'}}>
            <View style={styles.container}>
                
                <ImageBackground imageStyle={{resizeMode: 'stretch'}} source={images.loginTextBackground} style={styles.descriptionContainer}>
                    <Text style={styles.descriptionText}>SignUp with new account.</Text>
                </ImageBackground>
                <View style={styles.loginForm}>
                    <Input containerStyle = {styles.input} 
                        value={username}
                        autoCapitalize="none" 
                        returnKeyType="next" 
                        placeholder='User Name'
                        placeholderTextColor='#CCCCCC'
                        inputContainerStyle={{borderColor: "transparent"}}
                        inputStyle={{ color: 'white', fontSize: 16 }}
                        onChangeText={(data) => {this.handleLogUserNameInput(data)}}
                        leftIcon={{ type: 'font-awesome', name: 'user', color: 'white',marginRight: 6 }}/>

                    <Input containerStyle = {styles.input} 
                        value={email}
                        autoCapitalize="none" 
                        keyboardType='email-address' 
                        returnKeyType="next" 
                        placeholder='Email address'
                        placeholderTextColor='#CCCCCC'
                        inputContainerStyle={{borderColor: "transparent"}}
                        inputStyle={{ color: 'white', fontSize: 14 }}
                        onChangeText={(data) => {this.handleLogDataInput(data)}}
                        leftIcon={{ type: 'font-awesome', name: 'envelope', color: 'white',marginRight: 6 }}/>

                    <Input containerStyle = {styles.input}   
                        value={password}
                        returnKeyType="go" 
                        placeholder='Password' 
                        placeholderTextColor='#CCCCCC' 
                        secureTextEntry
                        underlineColorAndroid="transparent"
                        inputContainerStyle={{borderColor: "transparent"}}
                        inputStyle={{ color: 'white', fontSize: 14 }}
                        onChangeText={(data) => {this.handlePasswordInput(data)}}
                        leftIcon={{ type: 'font-awesome', name: 'lock', color: 'white',marginRight: 6 }}/>

                    <Input containerStyle = {styles.input}   
                        value={repassword}
                        returnKeyType="go" 
                        placeholder='Re-type Password' 
                        placeholderTextColor='#CCCCCC' 
                        secureTextEntry
                        underlineColorAndroid="transparent"
                        inputContainerStyle={{borderColor: "transparent"}}
                        inputStyle={{ color: 'white', fontSize: 14 }}
                        onChangeText={(data) => {this.handleRetypePasswordInput(data)}}
                        leftIcon={{ type: 'font-awesome', name: 'lock', color: 'white',marginRight: 6 }}/>

                    <Button disabled={password != repassword} buttonStyle={styles.signin} title="SIGN UP"  onPress={this.onSignUp.bind(this)}/>

                    <Button buttonStyle={styles.back} title="BACK" onPress={() => this.props.navigation.goBack()}/>
                </View>
            </View>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
    },
    descriptionContainer: {
        marginTop: 20,
        padding: 20,
    },
    descriptionText: {
        color: '#000000',
        fontSize: 30
    },
    loginForm: {
        width: '100%',
        padding: 50,
    },
    input:{
        height: 40,
        backgroundColor: 'rgba(128, 128, 128, 0.7)',
        marginBottom: 30,
        width: '100%',
        borderRadius: 10
    },
    signin:{
        backgroundColor: '#FE9723',
        marginBottom: 10,
        marginTop: 10
    },
    back:{
        backgroundColor: '#23ff89',
        marginBottom: 20,
        marginTop: 10
    },
    buttonText:{
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
    }
});

const mapStateToProps = store => ({
    tokenValue: store.tokenReducer,
    userValue: store.userReducer
});
  
const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(ActionCreators, dispatch);
}
  
export default connect(mapStateToProps, mapDispatchToProps)(SignUp);