import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, ImageBackground, ScrollView, TouchableOpacity } from 'react-native'
import { Input, Button } from 'react-native-elements';
import { width, height } from 'react-native-dimension';

import { ActionCreators } from '../../redux/actions/index'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import API from '../../api/api'
import images from '../../utils/images'

class Home extends Component {

  constructor(props) {
    super(props);
    
    this.state = {
      categories: []
    };

  }

  componentDidMount() {
    API.post("/category/getAllCategories").then(res => {
      // console.log(res);
      this.setState({"categories": res.data.doc});

    });
  }

  componentWillUnmount() {

  }

  _renderCategoryItem(itemData, key) {
    console.log(itemData);
    return (
      <TouchableOpacity style={styles.categoryItem} key={key}>
        <Text style={{ color: "Black", fontSize: 24, margin: 10 }}>{itemData.name}</Text>
      </TouchableOpacity>
    )
  }
  _renderCategoryList(){
    return (
      this.state.categories.map((itemData, key) => {
        return this._renderCategoryItem(itemData, key);
      })
    )
  }

  render() {
    return (
      <ImageBackground source={images.homeBackground} style={{width:'100%', height:'100%'}}>
        <View style={styles.container}>
          <View style={{ marginTop: 80, marginBottom: 30, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
            <Image
                source={images.logoImage}
                style={{height: height(8), width: width(80)}}
                resizeMode="stretch"
            />
          </View>
            <View style={styles.descriptionContainer}>
              <Text style={styles.descriptionText}>Select main category to view.</Text>
            </View>
          <View style={styles.itemContainer}>
            <ScrollView style={{width: "100%"}}>
            {
                this._renderCategoryList()
            }
            </ScrollView>
          </View>
        </View>
      </ImageBackground>
    )
  }
}

const styles = StyleSheet.create({
    container : {
      flex: 1,
      alignItems: 'center',
      width: '100%',
      height: '100%'
    },
    itemContainer : {
      flex: 1,
      width: '80%',
      height: '100%',
      backgroundColor: 'rgba(128, 128, 128, 0.2)',
      borderRadius: 20,
      marginBottom: 30,
      padding: 10
    },
    categoryItem : {
      width: '100%',
      height: 60,
      marginTop: 10,
      alignItems: 'center',
      borderRadius: 10,
      borderWidth: 2,
      borderColor: 'rgba(128, 128, 128, 1)',
      backgroundColor: 'rgba(256, 256, 256, 1)'
    }
});


const mapStateToProps = store => ({
  tokenValue: store.tokenReducer,
  userValue: store.userReducer,
});

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);