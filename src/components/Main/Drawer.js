//import liraries
import React, { Component } from 'react';
import { createDrawerNavigator , createStackNavigator} from 'react-navigation';
import { width } from 'react-native-dimension';

import Side_Bar from '../SideBar/Side_Bar';
import Home from '../Home/Home'

const getDrawerNavigator = Screen => createDrawerNavigator ({
    Main: {
        screen: Screen
    }
},{
    drawerWidth: width(80),
    drawerPosition: 'right',
    contentComponent: Side_Bar,
});

const Drawer = createStackNavigator({
    HomeScreen: {
        screen: getDrawerNavigator(Home),
        navigationOptions: {
            gesturesEnabled: true
        }
    }
},{
    initialRouteName: 'HomeScreen',
    headerMode: 'none'
})

export default Drawer;