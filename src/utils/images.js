const images = {

  // Logo Image 
  logoImage : require('../assets/images/home/name.png'),

  homeBackground: require('../assets/images/home/homeback.png'),

  // Login Screen
  loginBackground : require('../assets/images/login/login_back1.jpg'),
  loginTextBackground : require('../assets/images/login/textbg.png'),

  // Side bar
    userAvatar : require('../assets/images/sidebar/user_avatar.png'),
    sidebarBackground : require('../assets/images/sidebar/sideback.jpg'),
  // => ICONs
    sidebarIconHome : require('../assets/images/sidebar/home.png'),
    sidebarIconDiscovery : require('../assets/images/sidebar/discovery.png'),
    sidebarIconSearch : require('../assets/images/sidebar/search.png'),
    sidebarIconChat : require('../assets/images/sidebar/chat.png'),
    sidebarIconProfile : require('../assets/images/sidebar/profile.png'),
    sidebarIconSetting : require('../assets/images/sidebar/setting.png'),
    sidebarIconLogout : require('../assets/images/sidebar/logout.png'),

}

export default images;