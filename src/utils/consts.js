
var APP_ID = '847B905B-4EFE-490C-99A7-E831DCB2B09E';
var MILE = 1.60934;

export default {
    APP_ID: APP_ID,
    MILE: MILE,

    kmToMile: (length) => {
        if (!length)    return 0;
        return Math.round(length * 100 / MILE) / 100;
    }
}