
import isEmpty from 'lodash/isEmpty';
import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';
import { createStackNavigator, createAppContainer } from 'react-navigation';

import Login from './components/Login/Login';
import SignUp from './components/SignUp/SignUp';
import Drawer from './components/Main/Drawer';

const createStackNavigators = () => {
  return createStackNavigator({
    LoginScreen: {
        screen: Login
    },
    SignUpScreen: {
        screen: SignUp
    },
    Drawer: {
        screen: Drawer
    },
  }, {
    //   initialRouteName: loginReducer === false
    //       ? 'LoginScreen'
    //       : 'Drawer',
    initialRouteName: 'Drawer',
    headerMode: 'none'
  });
}

class Root extends Component {

  render() {
      const Navigator = createStackNavigators();
      const AppContainer = createAppContainer(Navigator);
      return (<AppContainer/>);
  }
}

const mapStateToProps = store => ({
    loginReducer: store.loginReducer,
});

export default connect(mapStateToProps)(Root);
