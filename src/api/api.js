import axios from 'axios';
import cofig from './config'

export default axios.create({           // The base url of external api
  baseURL: cofig.api_url_http
});