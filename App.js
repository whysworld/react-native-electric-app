import React from 'react';
import { StyleSheet, View } from 'react-native';
import Root from './src/Root'
import { Provider } from 'react-redux';
import createStore from './src/redux/createStore';

export default class App extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    const store = createStore();
    return (
      <Provider store={store}>
        <Root />
      </Provider>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
